# optms-idempotent

#### 介绍
* idempotent-demo 测试工程[幂等测试, 链路测试]
* optms-idempotent-spring-boot-starter 基于redisson starter的幂等注解
* optms-logger-spring-boot-starter 基于logback,slfj,TransmittableThreadLocal实现统一日志,业务链路跟踪
* request.http 测试api

#### 软件架构

https://github.com/redisson/redisson/

https://github.com/WangJi92/idempotent-spring-boot-starter

https://zhuanlan.zhihu.com/p/380530036