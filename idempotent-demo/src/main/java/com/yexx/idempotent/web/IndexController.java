package com.yexx.idempotent.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: index
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date: 2022/07/05-11:19
 */
@RestController
@RequestMapping
public class IndexController {

    @GetMapping
    public String index(){
        return "idempotent-demo-server";
    }

}
