package com.yexx.starter.uid.common;

import lombok.extern.slf4j.Slf4j;

import java.security.SecureRandom;
import java.util.Base64;

/**
 * Description: 随机工具
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date: 2022/07/20-14:40
 */
@Slf4j
public class RandomUtil {


    /**
     * 1. 提供能满足加密的强随机数生成器
     * 2. ramdom中seed是不可预测的
     *      一般使用默认的种子生成策略就行，对应 Linux 里面就是 /dev/random 和 /dev/urandom, 利用系统手机的随机事件作为seed值
     *      /dev/random : 依赖于系统产生的中断事件, 产生的速度比较慢, 可能会导致程序阻塞
     *      /dev/urandom : 产生的速度非常快
     * */
    private static final SecureRandom RANDOM = new SecureRandom();

    private static final String RANDOM_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    /**
     * 随机生成19位的Long
     */
    public static synchronized Long randomLong(){
        return Math.abs(RANDOM.nextLong());
    }

    /**
     * 随机生成base64加密的length长度的byte数组
     * @param length 数组长度
     */
    public static synchronized String randomBase64(int length) {
        byte[] randomBytes = new byte[length];
        RANDOM.nextBytes(randomBytes);
        return Base64.getEncoder().encodeToString(randomBytes);
    }

    /**
     * 随机生成长度未length的字符串
     * @param length 字符串长度
     */
    public static synchronized String randomString(int length) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 16; i++) {
            int number = RANDOM.nextInt(RANDOM_STRING.length());
            sb.append(RANDOM_STRING.charAt(number));
        }
        return sb.toString();
    }

}
