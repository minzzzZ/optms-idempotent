package com.yexx.starter.uid;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description: 全局uid配置
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date 2022/07/20-14:57
 */
@Data
@ConfigurationProperties(prefix = "app.uid")
public class UidProperties {

    private boolean enable = true;

    private String type = "snowflake";

    private Snowflake snowflake;

    @Data
    public static class Snowflake {

        /** 机房id (0~31) */
        private Long datacenterId = 0L;

        // todo 下一步 利用redis环形数组, [0~1024] 支持扩展, 解决多pod下ID重复问题
        /** 机器id (0~31) */
        private Long workerId = 0L;

    }

}
