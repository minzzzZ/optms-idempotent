package com.yexx.starter.uid.common;

import java.util.List;

/**
 * Description: uid生成器
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date: 2022/07/20-13:41
 */
public interface UidGenerator {

    /**
     * generate one
     */
    Long generate();

    /**
     * generate batch
     * @param size size
     */
    List<Long> batchGenerate(int size);

}
