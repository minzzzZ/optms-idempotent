package com.yexx.starter.uid;

import com.yexx.starter.uid.common.SnowFlakeIdGenerator;
import com.yexx.starter.uid.common.UidGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * Description: 配置
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date: 2022/07/20-13:38
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(value = {UidProperties.class})
public class UidAutoConfiguration {

    @Resource
    private UidProperties uidProperties;

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "app.uid", value = "enable", havingValue = "true", matchIfMissing = true)
    public UidGenerator uidGenerator(){
        UidProperties.Snowflake snowflake = uidProperties.getSnowflake();
        log.info("snowflake id generator initialization :) workerId : [{}], datacenterId : [{}]", snowflake.getWorkerId(), snowflake.getDatacenterId());
        return new SnowFlakeIdGenerator(snowflake.getWorkerId(), snowflake.getDatacenterId());
    }
}
