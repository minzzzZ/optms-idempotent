package com.yexx.starter.optms.idempotent;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

/**
 * description:幂等配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date 2022/06/01:22:17
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "app.idempotent")
public class IdempotentProperties {

    /** 不拦截的url, 逗号分割 */
    private String excludeUrls;

    /** 拦截的url , 逗号分割 */
    private String includeUrls = "/**";

    /** 拦截优先级 */
    private Integer idempotentInterceptorOrder = Ordered.LOWEST_PRECEDENCE;

    /** 默认请求头锁定key */
    private String defaultHeaderLockKey = "Authorization";

    /** 默认cookie锁定key */
    private String defaultCookieLockKey;

    /**
     * 手动配置拦截器
     */
    private Boolean manualSettingIdempotentInterceptor = false;
}
