package com.yexx.starter.optms.idempotent.keygen.impl;

import com.yexx.starter.optms.idempotent.annotation.Idempotent;
import com.yexx.starter.optms.idempotent.keygen.LockKeyGenerator;
import com.yexx.starter.optms.idempotent.utils.IpUtils;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * description:基于Ip机制生成锁的可以
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date 2022/06/01:22:49
 */
public class IpLockKeyResolver implements LockKeyGenerator {

    public IpLockKeyResolver() {
    }

    @Override
    public String resolver(Idempotent idempotent, HttpServletRequest request, HttpServletResponse response, HandlerMethod handlerMethod) {
        String ip = IpUtils.getIpAddress(request);
        return String.format("%s_%s", ip, request.getRequestURI());
    }
}