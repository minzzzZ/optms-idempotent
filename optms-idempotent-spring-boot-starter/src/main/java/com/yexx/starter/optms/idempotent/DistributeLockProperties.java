package com.yexx.starter.optms.idempotent;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * Description: 分布式锁配置
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date: 2022/07/13-17:47
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "app.lock")
public class DistributeLockProperties {

    /** 是否启用 */
    private boolean enable = false;

    /** 默认锁等待时间 不等待 */
    private Long defaultWaitTime = 0L;

    /** 默认锁失效时间 30s : 防止死锁 */
    private Long defaultExpireTime = 30L;

    /** 默认锁时间单位 秒 */
    private TimeUnit timeUnit = TimeUnit.SECONDS;

    /** 默认锁定键前缀-全局唯一 */
    private String defaultLockKeyPrefix = "common:lock";

    /** 是否支持续约- 默认不支持 */
    private boolean renew = false;

}
