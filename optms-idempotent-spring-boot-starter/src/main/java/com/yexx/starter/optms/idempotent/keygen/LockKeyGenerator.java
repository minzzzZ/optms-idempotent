package com.yexx.starter.optms.idempotent.keygen;

import com.yexx.starter.optms.idempotent.annotation.Idempotent;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * description: 锁的key生成器
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date 2022/06/01:22:39
 */
public interface LockKeyGenerator {

    /**
     * 没有找到值
     */
    String NOT_FOUND = "NOT_FOUND_VALUE";

    /**
     * 没有授权值
     */
    String NO_AUTH = "no_auth_value";

    String resolver(Idempotent idempotent, HttpServletRequest request, HttpServletResponse response, HandlerMethod method);

}
