package com.yexx.starter.optms.idempotent.annotation;

import com.yexx.starter.optms.idempotent.keygen.LockKeyGenerator;
import com.yexx.starter.optms.idempotent.keygen.impl.DefaultLockKeyResolver;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * description:幂等注解
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date 2022/06/01:22:24
 */
@Documented
@Inherited
@Target(ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Idempotent {

    /**
     * 有效期 default : 2
     */
    long expiredTime() default 2L;

    /**
     * 获取锁等待的时间 default : 0
     */
    long waitTime() default 0L;

    /**
     * 时间单位 default : 秒
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * 缓存key 前缀
     *
     * @return
     */
     String lockKeyPrefix() default "common:idempotent";

    /**
     * 错误码
     */
    int errCode() default 22222;

    /**
     * 错误信息
     */
    String errMsg() default "系统判定为重复提交, 请稍后重试!";

    /**
     * 是否解除当前key的锁定, 否则过期后才能继续点击
     */
    boolean unlockKey() default true;

    /**
     * lock key的生成策略
     */
    Class<? extends LockKeyGenerator> keyGenerator() default DefaultLockKeyResolver.class;

}
