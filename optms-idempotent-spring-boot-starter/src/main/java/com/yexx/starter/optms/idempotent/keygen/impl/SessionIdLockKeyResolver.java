package com.yexx.starter.optms.idempotent.keygen.impl;

import com.yexx.starter.optms.idempotent.annotation.Idempotent;
import com.yexx.starter.optms.idempotent.keygen.LockKeyGenerator;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * description:基于session机制生成锁的可以
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date 2022/06/01:22:49
 */
public class SessionIdLockKeyResolver implements LockKeyGenerator {

    public SessionIdLockKeyResolver() {
    }

    @Override
    public String resolver(Idempotent idempotent, HttpServletRequest request, HttpServletResponse response, HandlerMethod handlerMethod) {
        String sessionId = request.getSession().getId();
        if (!StringUtils.hasText(sessionId)) {
            sessionId = NOT_FOUND;
        }
        return String.format("%s_%s", sessionId, request.getRequestURI());
    }
}