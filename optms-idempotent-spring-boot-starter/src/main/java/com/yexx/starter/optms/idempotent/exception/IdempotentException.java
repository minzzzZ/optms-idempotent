package com.yexx.starter.optms.idempotent.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * description:异常
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date 2022/06/02:09:49
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class IdempotentException extends RuntimeException {

    private Integer errCode;

    public IdempotentException() {
        super();
    }

    public IdempotentException(Integer errCode, String errMsg) {
        super(errMsg);
        this.errCode = errCode;
    }
}

