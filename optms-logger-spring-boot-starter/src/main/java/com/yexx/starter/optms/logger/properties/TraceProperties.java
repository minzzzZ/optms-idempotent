package com.yexx.starter.optms.logger.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * Description: 是否启用trace
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date: 2022/06/26-20:00
 */
@Data
@ConfigurationProperties(prefix = "app.trace")
public class TraceProperties {

    /**
     * 是否启用跟踪 : 默认开启
     */
    private Boolean enable = true;

    /**
     * 追踪-请求头中的属性
     */
    private List<String> headers;

    /**
     * 追踪-url参数中的属性
     */
    private List<String> params;

    /**
     * 追踪-当前上下文中的属性
     */
    private List<String> contexts;

    public boolean enable() {
        return enable;
    }

}
