package com.yexx.starter.optms.logger.common;

/**
 * Description: 追踪常量
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date: 2022/06/26-20:47
 */
public final class TraceConstants {

    /** 追踪id : 链路跟踪id, 在header中传递 */
    public static final String TRACE_ID = "x-t-id";

    /** 用户id : 如果需要传递有状态 在header中传递 */
    public static final String USER_ID = "x-u-id";

    /** 租户id : 适用于租户模型, 在header中传递 */
    public static final String TENANT_ID = "x-tn-id";

}
