package com.yexx.starter.optms.logger;

import com.yexx.starter.optms.logger.filter.TraceFilter;
import com.yexx.starter.optms.logger.properties.TraceProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

/**
 * Description: 日志自动配置
 *
 * @author zuomin (myleszelic@outlook.com)
 * @date: 2022/06/26-17:08
 */
@Slf4j
@Configuration
@EnableConfigurationProperties({TraceProperties.class})
public class LoggerAutoConfiguration {

    @Bean
    @ConditionalOnWebApplication
    @ConditionalOnClass({Filter.class})
    public TraceFilter traceFilter(TraceProperties traceProperties) {
        log.info("trace point : headers : [{}], params : [{}], contexts : [{}]",
                traceProperties.getHeaders(), traceProperties.getParams(), traceProperties.getContexts());
        return new TraceFilter(traceProperties);
    }

}
