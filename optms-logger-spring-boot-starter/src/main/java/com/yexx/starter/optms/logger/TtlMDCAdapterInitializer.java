package com.yexx.starter.optms.logger;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.slf4j.TtlMDCAdapter;

/**
 * Description: 初始化TtlMDCAdapter实例，并替换MDC中的adapter对象
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2020/12/02-18:14
 */
public class TtlMDCAdapterInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        //加载TtlMDCAdapter实例
        TtlMDCAdapter.getInstance();
    }
}
